import urllib3
import pandas
url = "http://aima.cs.berkeley.edu/data/iris.csv" ## Define datasource
conn = urllib3.PoolManager().request('GET', url) ## Get Data.
file = open('iris.csv','w') ## Create file
file.write(conn.data.decode('UTF-8')) ## Write data to file.
file.close() ## Close file.
## Read data file as CSV.
iris = pandas.read_csv('iris.csv', sep=',', decimal='.',header=None, names= ['sepal_length', 'sepal_width','petal_length', 'petal_width', 'target'])
print(iris.columns) ## Print columns.
print(iris.head()) ## Print first five rows.
print(iris.tail()) ## Print last five rows.
print(iris.head(2)) ## Print first n rows.
print(iris.tail(2)) ## Print last n rows
print(iris['sepal_length'].head(2)) ## Pick a column and print a subset.
print(iris[['sepal_width', 'petal_length']].head(7)) ## Pick multiple columns and print a subset.
print(type(iris[['sepal_width', 'petal_length']].head(7))) ## Multiple columns are stored in a pandas.DataFrame object.
print(type(iris['sepal_length'].head(2))) ## Single columns are stored in a pandas.Series object.

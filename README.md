# datascience-virtualenv
A Python 3 virtualenv sandbox for various Data science experiments.

## Prerequisites.

This project uses Python3 exclusively. We'll need pip3 installed and in PATH.

`brew install python3` on macOS.

Install all necessary packages using the `requirements.txt` in this repo. 

`pip3 install -U -r requirements.txt` 

Create a new virtualenv for the project and change directory.

```bash
virtualenv datascience-virtualenv 
cd datascience-virtualenv
```

Start a local instance of Jupyter.

`jupyter notebook`

A browser will open up with the Jupyter homepage. Create a fresh notebook and you should be good to go. Happy coding!
